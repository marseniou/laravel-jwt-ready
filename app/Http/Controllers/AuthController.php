<?php

namespace App\Http\Controllers;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Laravel\Socialite\Facades\Socialite;



class AuthController extends Controller
{
    public function __construct(){
        $this->middleware('auth:api', ['only'=>['user']]);
    }
    public function register(Request $request)
    {
        $this->validate($request, [
        'name' => 'required',
        'password' => 'required',
        'email' => 'required|email|unique:users'
    ]);
        $user = User::create([
             'email'    => $request->email,
             'password' => $request->password,
             'name' => $request->name
         ]);

        $token = Auth::login($user);
        $user->sendApiEmailVerificationNotification();

        return $this->respondWithToken($token);
    }
    public function register_social(Request $request)
    {
        $r = User::where('email', $request->email)->first();

        if(is_null($r)){
        $user = User::create([
             'email'    => $request->email,
             'password' => Str::random(10),
             'name' => $request->name,
             'email_verified_at'=> Carbon::now()
         ]);    
    } else {
        $user = $r;
    }
        

        $token = Auth::login($user);
        //$user->sendApiEmailVerificationNotification();

        return $this->respondWithToken($token);
    }
    public function login(Request $request)
    {

        $credentials = $request->only(['email', 'password']);

        if (! $token = Auth::attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    public function logout()
    {
        Auth::logout();

        return response()->json(['message' => 'Successfully logged out']);
    }


    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type'   => 'bearer',
            'expires_in'   => auth()->factory()->getTTL() * 60
        ]);
    }
    public function user(){
        $user =Auth::user();
        $user->setAttribute('hash', md5($user->email));
        return response()->json(['user'=>$user]);
    }
    public function get_events(){
        return config('events');
    }
 
}