<?php

use Illuminate\Support\Facades\DB;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    $locations = DB::table('locations')->get();
    foreach ($locations as $key => $location) {
    	$l = explode(',',$location->alternatenames);

    	DB::table('locations')->where('geonameid',$location->geonameid)->update(['alternatenames'=>end($l)]);
    }
});*/

Auth::routes(['verify'=>true]);

Route::get('/home', 'HomeController@index')->name('home');
