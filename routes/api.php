<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


/*Route::middleware('auth:api')->post('/auth/login', [
	'uses'=>'AuthController@login'
]);*/

Route::get('email/verify/{id}', 'VerificationApiController@verify')->name('verificationapi.verify');
Route::get('email/resend', 'VerificationApiController@resend')->name('verificationapi.resend');
Route::post('/auth/register', 'AuthController@register');
Route::post('/auth/register_', 'AuthController@register_social');

Route::post('/auth/login', 'AuthController@login');
Route::post('auth/logout', 'AuthController@logout');
Route::get('/user', 'AuthController@user');
Route::get('/get_events', 'AuthController@get_events');

Route::post('event/create', 'EventController@store');
