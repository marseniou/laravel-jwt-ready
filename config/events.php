<?php 

return [
	'cities' => [
		[
			'text'=>'Λάρισα',
			'value' => 1
		],
		[
			'text'=>'Βόλος',
			'value' => 2
		],
		[
			'text'=>'Τρίκαλα',
			'value' => 3
		],
		[
			'text'=>'Καρδίτσα',
			'value' => 4
		],
		[
			'text'=>'Τύρναβος',
			'value' => 5
		],
		[
			'text'=>'Αλμυρός',
			'value' => 6
		],
		[
			'text'=>'Ελασσόνα',
			'value' => 7
		],
		[
			'text'=>'Παλαμάς',
			'value' => 8
		],
		[
			'text'=>'Καλαμπάκα',
			'value' => 9
		],
		[
			'text'=>'Σοφάδες',
			'value' => 10
		],
		[
			'text'=>'Φάρσαλα',
			'value' => 11
		],
	],
	'categories' => [
		[
		'text'=>'Μουσική',
		'value' => 1
		],
		[
		'text'=>'Party',
		'value' => 2
		],
		[
		'text'=>'Φαγητό',
		'value' => 3
		],
		[
		'text'=>'Παραδοσιακά',
		'value' => 4
		],
		[
		'text'=>'Θέατρο',
		'value' => 5
		],
		[
		'text'=>'Κινηματογράφος',
		'value' => 6
		],
		[
		'text'=>'Χορός',
		'value' => 7
		],
	]
];